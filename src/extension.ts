import * as vscode from 'vscode';
import { Config } from './main/config';
import { Decoration } from './main/decoration';
import { HttpUtil } from './main/marketapi/httputil';
import { OverviewCommands } from './main/overview/overviewcommands';
import { OverviewConfig } from './main/overview/overviewconfig';
import { OverviewProvider } from './main/overview/overviewprovider';
import { ViewProviders } from './main/viewProviders';
import { WatchlistCommands } from './main/watchlist/watchlistcommands';
import { WatchlistConfig } from './main/watchlist/watchlistconfig';

export async function activate(context: vscode.ExtensionContext) {

	Config.logger.log("Starting...");

	await HttpUtil.setup();
	Config.loadConfig();
	WatchlistConfig.loadConfig();
	await OverviewConfig.loadConfig();
	ViewProviders.setViews();
	WatchlistCommands.setCommands(context);
	OverviewCommands.setCommands(context);

	vscode.window.registerFileDecorationProvider(new Decoration());
	
	Config.logger.log("Loaded...");
}

export function deactivate() {}
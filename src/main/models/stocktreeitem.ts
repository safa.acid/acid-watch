import { cpuUsage } from 'node:process';
import * as vscode from 'vscode';
import { Config } from '../config';
import { MarketAPI } from '../marketapi/marketapi';
import { Stock } from './stock';
import { StockObj } from './stockobj';
import path = require('path');

export class StockTreeItem extends vscode.TreeItem {

    constructor(
        public readonly content: StockObj,
    ) {        
        super(content.label, vscode.TreeItemCollapsibleState.Collapsed);
        let self = this;
        let uriStr = "acidwatch://StockTreeItem?";
        if(content.stockResponse.diff >=0 ){
            uriStr += "&trend=positive";
        }
        uriStr += "&live="+content.stockResponse.live_price;
        this.resourceUri = vscode.Uri.parse(uriStr);
        this.tooltip = content.label;        
        this.label = content.label;
        this.iconPath = {
            light: path.join(__filename, '..', '..', 'resources', 'icons', 'light', 'tag.svg'),
            dark: path.join(__filename, '..', '..', 'resources', 'icons', 'dark', 'tag.svg')
        };
    }

    contextValue = 'StockTreeItem';
}
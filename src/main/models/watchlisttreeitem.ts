import * as vscode from 'vscode';
import { Watchlist } from './watchlist';

export class WatchlistTreeItem extends vscode.TreeItem {

    constructor(
        public readonly content: Watchlist,
    ) {        
        super(content.name, vscode.TreeItemCollapsibleState.Expanded);
        this.label = content.name;
    }

    contextValue = 'WatchlistTreeItem';
}
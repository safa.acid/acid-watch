export interface StockResponse {
    prev_close : number,
    open : number,
    last_price : number,
    close : number,
    parsed : boolean,
    diff : number,
    diff_pnt : number,
    live_price : number
}
import { MarketAPI } from "../marketapi/marketapi";
import { Stock } from "./stock";
import { StockResponse } from "./stockresponse";

export class StockObj {

    label: string;
    stockResponse: StockResponse;

    constructor (
        public readonly content: Stock
    ) {        
        this.label = content.symbol;
        this.stockResponse = {
            close : 0,
            prev_close :  0,
            last_price : 0,
            open : 0,
            parsed : false,
            diff : 0,
            diff_pnt : 0,
            live_price : 0
        };
    };

    public async setup(){
        this.stockResponse = await MarketAPI.getStockData(this.content);
        this.parseLabel();
    }

    public parseLabel(){        
        if(this.stockResponse.parsed){
            let prev_close = this.stockResponse.prev_close;
            let close = this.stockResponse.close;
            if(close == 0){
                close = this.stockResponse.last_price;
            }
            this.stockResponse.live_price = close;
            let diff = (close - prev_close).toFixed(2);
            let diff_percentage = (((close - prev_close)/prev_close)*100).toFixed(2);
            this.stockResponse.diff = parseFloat(diff);
            this.stockResponse.diff_pnt = parseFloat(diff_percentage);
            this.label += " :: " + diff + " :: "+ diff_percentage+"%";
        }else{
            this.label += " Error ";
        }
    }
}
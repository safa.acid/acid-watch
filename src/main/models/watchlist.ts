import { Stock } from "./stock";

export interface Watchlist {
    name: string,
    list: Array<Stock>;
}
import * as vscode from 'vscode';
import { Logger } from '../logger';
import { MarketAPI } from '../marketapi/marketapi';
import { Watchlist } from '../models/watchlist';

export class OverviewConfig {

    static logger = new Logger();

    private static watchlists: Array<Watchlist> = [];

    public static async loadConfig() {
        this.logger.log("Loading Config");
        
        this.watchlists = [
            {
                name : 'NSE Top Gainers',
                list : await MarketAPI.nseTop(1)
            },
            {
                name : 'NSE Top Losers',
                list : await MarketAPI.nseTop(2)
            }
        ];

        this.logger.log("Loading Config - Completed");
    }

    public static getConfig(){
        return this.watchlists;
    }
    
    

}
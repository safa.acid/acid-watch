import * as vscode from 'vscode';
import { ViewProviders } from '../viewProviders';

export class OverviewCommands {

    static setCommands(context: vscode.ExtensionContext) {

        context.subscriptions.push(vscode.commands.registerCommand('acid-watch.overview.refresh', async (args) => {
            ViewProviders.overviewProvider.refreshUI(undefined);
        }));

    }

}
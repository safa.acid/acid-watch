import * as vscode from 'vscode';
import { OverviewProvider } from './overview/overviewprovider';
import { WatchlistProvider } from './watchlist/watchlistprovider';

export class ViewProviders {
    static watchlistProvider: WatchlistProvider;
    static overviewProvider: OverviewProvider;

    static watchlistView: vscode.TreeView<any>;
    static overviewView: vscode.TreeView<any>;

    static setViews(): void {

        ViewProviders.watchlistProvider = new WatchlistProvider();
        ViewProviders.watchlistView = vscode.window.createTreeView('acid-watch-watchlist', {
            treeDataProvider: ViewProviders.watchlistProvider
        });

        ViewProviders.overviewProvider = new OverviewProvider();
        ViewProviders.watchlistView = vscode.window.createTreeView('acid-watch-overview', {
            treeDataProvider: ViewProviders.overviewProvider
        });
    }
}
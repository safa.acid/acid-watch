import * as vscode from 'vscode';
import { ViewProviders } from '../viewProviders';

export class WatchlistCommands {

    static setCommands(context: vscode.ExtensionContext) {

        context.subscriptions.push(vscode.commands.registerCommand('acid-watch.watchlist.refresh', async (args) => {
            ViewProviders.watchlistProvider.refreshUI(undefined);
        }));

    }

}
import * as vscode from 'vscode';
import { StockObj } from '../models/stockobj';
import { StockTreeItem } from '../models/stocktreeitem';
import { WatchlistTreeItem } from '../models/watchlisttreeitem';
import { WatchlistConfig } from './watchlistconfig';

export class WatchlistProvider implements vscode.TreeDataProvider<vscode.TreeItem> {

    public _onDidChangeTreeData: vscode.EventEmitter<vscode.TreeItem | undefined> = new vscode.EventEmitter<vscode.TreeItem | undefined>();
    readonly onDidChangeTreeData: vscode.Event<vscode.TreeItem | undefined> = this._onDidChangeTreeData.event;

    refreshUI(element: undefined | vscode.TreeItem) {
        this._onDidChangeTreeData.fire(element);
    }

    getTreeItem(element: any): vscode.TreeItem | Thenable<vscode.TreeItem> {
        return element;
    }
    getChildren(element?: any): vscode.ProviderResult<any[]> {
        if (!element) {
            let watchlists = WatchlistConfig.getConfig();
            let ret: WatchlistTreeItem[] = [];
            watchlists.forEach(function (watchlist) {
                ret.push(new WatchlistTreeItem(watchlist));
            });
            return Promise.resolve(ret);
        } else if(element instanceof WatchlistTreeItem) {
            return this.getStocks(element);
        } else if(element instanceof StockTreeItem){
            let ret: vscode.TreeItem[] = [];
            ret.push(new vscode.TreeItem("Prev Close : "+element.content.stockResponse.prev_close, vscode.TreeItemCollapsibleState.None));
            ret.push(new vscode.TreeItem("Open : "+element.content.stockResponse.open, vscode.TreeItemCollapsibleState.None));
            ret.push(new vscode.TreeItem("Current : "+element.content.stockResponse.last_price, vscode.TreeItemCollapsibleState.None));
            ret.push(new vscode.TreeItem("Close : "+element.content.stockResponse.close, vscode.TreeItemCollapsibleState.None));
            return ret;
        } else {
            return Promise.resolve([]);
        }
    }

    private async getStocks(element: WatchlistTreeItem): Promise<Array<vscode.TreeItem>> {
        return new Promise(async (resolve, reject) => {
            let self = this;
            let list = element.content.list;
            let ret: StockTreeItem[] = [];
            for(var i=0;i<list.length;i++){
                let stockObj = new StockObj(list[i]);
                await stockObj.setup();
                ret.push(new StockTreeItem(stockObj));
            }
            resolve(ret);
        });
    }
}
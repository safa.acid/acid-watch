import * as vscode from 'vscode';
import { Logger } from '../logger';
import { Watchlist } from '../models/watchlist';

export class WatchlistConfig {

    static logger = new Logger();

    private static watchlists: Array<Watchlist> = [];

    public static loadConfig() {
        this.logger.log("Loading Config");
        let conf = vscode.workspace.getConfiguration("acid-watch");
        if (conf.watchlists) {
            this.watchlists = conf.watchlists;
        }else{
            this.watchlists = [
                {
                    name : 'dummy 1',
                    list : [
                        {
                            symbol : 'NSE:ASIANPAINT'
                        }
                    ]
                },
                {
                    name : 'dummy 2',
                    list : [
                        {
                            symbol : 'NSE:ITC'
                        },
                        {
                            symbol : 'NSE:RAILTEL'
                        },
                        {
                            symbol : 'NSE:PNCINFRA'
                        },
                        {
                            symbol : 'NSE:BIOCON'
                        }
                    ]
                }
            ]
        }
        this.logger.log("Loading Config - Completed");
    }

    public static getConfig(){
        return this.watchlists;
    }
    
    

}
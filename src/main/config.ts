import * as vscode from 'vscode';
import { Logger } from './logger';

export class Config {

    //Global Logger
    static logger = new Logger();

    //Conf Values
    private static conf_ignore_cert_err: boolean = false;

    public static loadConfig() {
        this.logger.log("Loading Config");
        let conf = vscode.workspace.getConfiguration("acid-watch");        
        Config.conf_ignore_cert_err = conf.ignoreCertErr;
        this.logger.log('ignore cert err : ' + Config.conf_ignore_cert_err);
        this.logger.log("Loading Config - Completed");
    }

    public static getIgnoreCertErr(): boolean {
        return Config.conf_ignore_cert_err;
    }

}
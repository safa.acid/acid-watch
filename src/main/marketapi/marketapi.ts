import * as vscode from 'vscode';
import { Config } from '../config';
import { QueryParam } from '../models/queryparam';
import { Stock } from '../models/stock';
import { StockResponse } from '../models/stockresponse';
import {HttpUtil} from './httputil';

export class MarketAPI {
    static async getStockData(stock: Stock) : Promise<StockResponse> {
        if(stock.symbol.includes("NSE:")){
            let queryparams:Array<QueryParam> = [];            
            queryparams.push({key:'symbol',value:stock.symbol.substring(4)});
            const response = await HttpUtil.get('https://www.nseindia.com/api/quote-equity',queryparams);
            return MarketAPI.parseNSE(response);
        }else {
            let queryparams:Array<QueryParam> = [];
            queryparams.push({key:'symbol',value:stock.symbol.substring(4)});
            const response = await HttpUtil.get('https://www.nseindia.com/api/quote-equity',queryparams);
            return MarketAPI.parseNSE(response);
        }
    }

    static parseNSE(response: any) : StockResponse {
        if(response.success){        
            let data = response.result.data;
            let ret:StockResponse = {
                close : data.priceInfo.close,
                prev_close :  data.priceInfo.previousClose,
                last_price : data.priceInfo.lastPrice,
                open : data.priceInfo.open,
                parsed : true,
                live_price : 0,
                diff : 0,
                diff_pnt : 0
            };
            return ret;
        }else{
            let ret:StockResponse = {
                close : 0,
                prev_close :  0,
                last_price : 0,
                open : 0,
                parsed : false,
                live_price : 0,
                diff : 0,
                diff_pnt : 0
            };
            return ret;
        }
    }

    static async nseTop(type:number): Promise<Stock[]> {
        let queryparams:Array<QueryParam> = [];         
        let url = 'https://www1.nseindia.com/live_market/dynaContent/live_analysis/gainers/fnoGainers1.json';
        if(type === 1){
            url = 'https://www1.nseindia.com/live_market/dynaContent/live_analysis/gainers/fnoGainers1.json';
        }else{
            url = 'https://www1.nseindia.com/live_market/dynaContent/live_analysis/losers/fnoLosers1.json';
        }
        const response = await HttpUtil.get(url,queryparams);
        let ret:Stock[] = [];
        if(response.success){
            let data = response.result.data.data;
            for(var i=0;i<data.length;i++){
                ret.push({symbol:'NSE:'+data[i].symbol});
            }
        }
        return ret;
    }
}
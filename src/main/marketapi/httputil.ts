import * as vscode from 'vscode';
import axios, { AxiosInstance } from 'axios';
import * as https from 'https';
import { Config } from '../config';
import { QueryParam } from '../models/queryparam';
import axiosCookieJarSupport from 'axios-cookiejar-support';
const tough = require('tough-cookie');

export class HttpUtil {

    static instance:AxiosInstance;
    static cookieJar:any;

    static async setup(){
        axiosCookieJarSupport(axios);
        HttpUtil.cookieJar = new tough.CookieJar();
        await this.setupCookies();
    } 
    
    static async setupCookies(){
        await HttpUtil.get('https://www.nseindia.com/',[]);
    }

    static async get(myurl:string, queryparams:Array<QueryParam>): Promise<any> {
        let firstParam = true;
        queryparams.forEach(function(queryparam){
           if(firstParam){
               myurl += "?";
               firstParam = false;
           } else{
               myurl += "&";
           }
           myurl += queryparam.key +"="+queryparam.value;
        });
        return this.http("GET", myurl)
    }

    static async http(method: "GET" | "POST", myurl: string): Promise<any> {
        var self = this;
        return new Promise((resolve, reject) => {
            
            if (!HttpUtil.instance){
                HttpUtil.instance = axios.create({
                withCredentials: true
              });
            }

            Config.logger.log('http :: ' + myurl);
            HttpUtil.instance.request({
                method: method,
                url: myurl,
                jar: HttpUtil.cookieJar,
                withCredentials: true
            }).then(function (json) {
                resolve({
                    'success': true,
                    'result': json
                });
            }).catch(async function (error) {
                if(error.response.status == 401){
                    await self.setupCookies();
                }
                Config.logger.log(error);
                resolve({
                    'success': false,
                    'result': error
                });
            });
        });
    }
}
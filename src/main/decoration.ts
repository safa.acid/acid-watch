import { URLSearchParams } from 'url';
import * as vscode from 'vscode';
import { CancellationToken, FileDecoration, ProviderResult, Uri } from 'vscode';

export class Decoration implements vscode.FileDecorationProvider{   

    provideFileDecoration(uri: Uri, token: CancellationToken): ProviderResult<FileDecoration>{
        if(uri.scheme === "acidwatch"){
            if(uri.authority === "StockTreeItem"){
                if(uri.query.includes("trend=positive")){
                    return {
                        badge: "⇧",
                        tooltip: "Trend",
                        color: new vscode.ThemeColor('acidwatch.green'),
                    };
                }else{
                    return {
                        badge: "⇩",
                        tooltip: "Trend",
                        color: new vscode.ThemeColor('acidwatch.red'),
                    };
                }
            }
        }
    }

}